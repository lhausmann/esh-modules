#include <Arduino.h>
#include <U8g2lib.h>

// white pcb series
U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ D0, /* clock=*/ D1, /* data=*/ D2);

// black pcb series
//U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ D2, /* clock=*/ D5, /* data=*/ D4);

#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <string>

ESP8266WiFiMulti WiFiMulti;

void setup(void) {
    Serial.begin(9600);
    delay(10);
    u8g2.begin();
    u8g2.setFont(u8g2_font_6x10_mf);
    u8g2.setFontMode(0);
    WiFi.mode(WIFI_STA);
    WiFiMulti.addAP("MSTN-WS-N", "DEADC0FFEE1234567890DEC0DE");
    pinMode(D3, INPUT_PULLUP);
    pinMode(D6, INPUT_PULLUP);
    pinMode(D7, INPUT_PULLUP);
    pinMode(D8, INPUT_PULLUP);
}

uint16_t adcval = 300;
unsigned char contrast = 0;

bool state = false;
bool tmp = false;

void loop(void) {
    u8g2.firstPage();
    WiFiClient client;
    HTTPClient http;

    u8g2.setContrast(contrast);

    String payload = "";

    do {
        char macbuf[WiFi.macAddress().length()+1];
        WiFi.macAddress().toCharArray(macbuf, sizeof(macbuf));
        u8g2.drawStr(0, 7, macbuf);

        if (WiFiMulti.run() == WL_CONNECTED){
            char ipbuf[WiFi.localIP().toString().length()+1];
            WiFi.localIP().toString().toCharArray(ipbuf, sizeof(ipbuf));
            u8g2.drawStr(0, 17, ipbuf);
            u8g2.drawStr(0, 27, "api rq...");

            if (!digitalRead(D3)){
                if (!tmp){
                    tmp = true;
                    state = !state;
                    String url = String("http://kaby.fritz.box:5000/api") + "?mac=" + WiFi.macAddress() + "&datatype=bool"
                    +  "&sensortype=ESP8266_simple_sensor" + "&value=" + state + "&displayunit=nA";
                    if(http.begin(client, url)) {
                        int httpCode = http.GET();
                        if(httpCode > 0) {
                            Serial.printf("HTTP Response Code: %d\n", httpCode);
                            if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
                                u8g2.drawStr(0, 27, "api http ok");
                            } else {
                                u8g2.drawStr(0, 27, "api http error");
                            }
                        }
                        http.end();
                    } else {
                        u8g2.drawStr(0, 27, "api connect fail");
                    }
                }
            }
            else if (tmp) {
                tmp = false;
            }
        }
        else{
            u8g2.drawStr(0, 17, "WiFi connecting...");
        }
        delay(50);
        //delay(60000);

    } while ( u8g2.nextPage() );
}
